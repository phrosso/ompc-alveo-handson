## **OMPC Basic usage**
This is intended to show the basic usage of OMPC.
- OMPC makes use of target directives to offload computation to other nodes. More information about OMPC is available on the documentation (https://ompcluster.readthedocs.io/en/latest/)
- We also keep a repository of examples one can try with OMPC: https://gitlab.com/ompcluster/ompc-examples

### **Refer to [Compiling ALVEO + OMPC](../readme.md#compiling-ompc-and-ompcalveo-fork) before going through this part**

### **Quick Start**
To directly run the examples, assuming having OMPC built:

```bash
# Make sure env.sh is properly set
source env.sh
make
make test
```

### **Environment**
For running OMPC, we need to set MPI (if not by default) and OMPC to the PATHs *(env.sh)*
```bash
# MPI
MPI_ROOT=<path_to_mpi_instalation>
export PATH=$MPI_ROOT/bin:$PATH
export LD_LIBRARY_PATH=$MPI_ROOT/lib:$LD_LIBRARY_PATH
# OMPC
OMPC_ROOT=<path_to_llvm_project>
export CXX=$OMPC_ROOT/build/bin/clang++
export LD_LIBRARY_PATH=$OMPC_ROOT/build/lib/:$OMPC_ROOT/build/projects/openmp/libomptarget:$LD_LIBRARY_PATH
export LIBRARY_PATH=$OMPC_ROOT/build/lib/:$OMPC_ROOT/build/projects/openmp/libomptarget:$LIBRARY_PATH
export CPATH=$OMPC_ROOT/build/projects/openmp/runtime/src:$CPATH
```

### **Element-Wise Vector Addition**
To show the basic usage of OMPC, let's do a Element-Wise vector addition of two arrays, storing in a third one. A very basic way do to it would be: 

```c++
void vadd(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++)
    C[i] = A[i] + B[i];
}
```
Assuming we have A, B and C initialized, a very simple kernel call would be:

```c++
#pragma omp parallel
#pragma omp single
  {
    #pragma omp target map(to: A[:size], B[:size]) map(tofrom: C[:size]) \
                       depend(inout : *A, *B, *C) nowait
        vadd(A, B, C, size);
  }
```

OMPC uses asynchronous target functions inside a parallel single region. The task execution only starts after the single region reached the end of it (or with other kind of wait mechanism, such as taskwait). This permits OMPC to have the task-graph of the region in hands to be able to schedule the tasks according to the nodes launched. **(notice the 'x86_64-pc-linux-gnu' target passed to the clang compiler)**

To run the code: **(We always launch OMPC with `mpirun`, that is how libomptarget detect we are using OMPC. We always use 1 process for the head node, and the rest for OMPC worker nodes).**
```bash
source env.sh
$CXX -fopenmp -fopenmp-targets=x86_64-pc-linux-gnu -fno-openmp-new-driver vadd.cpp -o vadd
mpirun -np 2 ./vadd
```

**PS-1:** To see what is happening inside the OMPC plugin one can enable the plugin debug messages:

- `-DLIBOMPTARGET_ENABLE_DEBUG=On` during the compilation of the LLVM fork
- `export OMPCLUSTER_DEBUG=1` before program execution

### **Diving into OMPC**

Please look at the OMPC documentation for tuning options and usage of OMPC: https://ompcluster.readthedocs.io/en/latest/index.html
