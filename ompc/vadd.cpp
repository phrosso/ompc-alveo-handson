#include <stdio.h>
#include <stdlib.h>

void vadd(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++)
    C[i] = A[i] + B[i];
}

void init_array(unsigned int *V, size_t size, unsigned int value) {
  for (int i = 0; i < size; i++)
    V[i] = value;
}

bool validate(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++)
    if (C[i] != A[i] + B[i])
      return false;
  return true;
}

int main() {
  size_t size = 1024;

  unsigned int *A = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(A, size, 1);
  unsigned int *B = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(B, size, 2);
  unsigned int *C = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(C, size, 0);

#pragma omp parallel
#pragma omp single
  {
#pragma omp target map(to: A[:size], B[:size]) map(tofrom: C[:size]) \
                   depend(inout: *A, *B, *C) nowait
    vadd(A, B, C, size);
  }

  if (!validate(A, B, C, size)) {
    printf("Exacution Failed\n"); 
    return 1;
  } else {
    printf("Exacution Succedeed\n"); 
    return 0;
  }
}