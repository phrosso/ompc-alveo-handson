#! /bin/bash
# MPICH
export PATH=/proj/xlabs_t3/users/pedrohen/mpich-nemesis/bin:$PATH
export LD_LIBRARY_PATH=/proj/xlabs_t3/users/pedrohen/mpich-nemesis/lib:$LD_LIBRARY_PATH
# OMPC
OMPC_DIR=/scratch/users/pedrohen/benchmarks/llvm-project
export CXX=$OMPC_DIR/build/bin/clang++
export LD_LIBRARY_PATH=$OMPC_DIR/build/lib/:$OMPC_DIR/build/projects/openmp/libomptarget:$LD_LIBRARY_PATH
export LIBRARY_PATH=$OMPC_DIR/build/lib/:$OMPC_DIR/build/projects/openmp/libomptarget:$LIBRARY_PATH
export CPATH=$OMPC_DIR/build/projects/openmp/runtime/src:$CPATH
