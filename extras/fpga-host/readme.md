## **FPGA-HOST**
This is intended to show how a basic EW-Vector addition host code would be with XRT API only. It is only for a single node, and made for comparison with ALVEO + OMPC. It is expected for these host codes to get more complex when involving multiple FPGA devices and even more when working on distributed environments.


### **Quick Start**
```bash
# Make sure env.sh is properly set
source env.sh
make
make test
```