#include <stdio.h>
#include <cassert>

#include "xrt/xrt_bo.h"
#include "xrt/xrt_device.h"
#include "xrt/xrt_kernel.h"
#include <experimental/xrt_xclbin.h>

void init_array(unsigned int *V, size_t size, unsigned int value) {
  for (int i = 0; i < size; i++)
    V[i] = value;
}

bool validate(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++)
    if (C[i] != (A[i] + B[i]))
      return false;
  return true;
}

int main() {
  size_t size = 1024;

  // Host buffers that will be mapped to FPGA need to be align allocated
  unsigned int *A;
  assert(!posix_memalign((void **)&A, 4096, size * sizeof(unsigned int)));
  init_array(A, size, 1);
  unsigned int *B;
  assert(!posix_memalign((void **)&B, 4096, size * sizeof(unsigned int)));
  init_array(B, size, 2);
  unsigned int *C;
  assert(!posix_memalign((void **)&C, 4096, size * sizeof(unsigned int)));
  init_array(C, size, 0);

  // FPGA Device Setup
  xrt::device dev(0);
  auto file_info = xrt::xclbin("./vadd.xclbin");
  auto uuid = dev.load_xclbin(file_info);
 
  // Setup FPGA buffers
  xrt::bo A_bo = xrt::bo(dev, A, size * sizeof(unsigned int), xrt::bo::flags::normal, 0);
  xrt::bo B_bo = xrt::bo(dev, B, size * sizeof(unsigned int), xrt::bo::flags::normal, 0);
  xrt::bo C_bo = xrt::bo(dev, C, size * sizeof(unsigned int), xrt::bo::flags::normal, 0);

  // Sync HOST -> FPGAs
  A_bo.sync(XCL_BO_SYNC_BO_TO_DEVICE);
  B_bo.sync(XCL_BO_SYNC_BO_TO_DEVICE);
  C_bo.sync(XCL_BO_SYNC_BO_TO_DEVICE);

  // Get FPGA kernel and run object
  xrt::kernel k(dev, uuid.get(), "vadd_hw");
  xrt::run r(k);

  // Set arguments
  r.set_arg(0, A_bo);
  r.set_arg(1, B_bo);
  r.set_arg(2, C_bo);
  r.set_arg(3, size);

  // Start kernel run and wait for it to complete
  r.start();
  r.wait();

  C_bo.sync(XCL_BO_SYNC_BO_FROM_DEVICE);

  if (!validate(A, B, C, size)) {
    printf("Execution Failed\n"); 
    return 1;
  } else {
    printf("Execution Succedeed\n"); 
    return 0;
  }
}