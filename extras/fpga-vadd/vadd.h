/**
 * Author: Pedro Henrique Rosso
 */

#include <ap_int.h>
#include <stddef.h>

constexpr unsigned int DEPTH_ = 131072;

using buffer_ty = unsigned int;
using wide_ty = ap_uint<512>;

void vadd_hw(buffer_ty *A, buffer_ty *B, buffer_ty *C, size_t size);