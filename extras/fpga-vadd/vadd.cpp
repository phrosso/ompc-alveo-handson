/**
 * Author: Pedro Henrique Rosso
 */

#include <ap_int.h>

#include "vadd.h"

void vadd_hw(buffer_ty *A, buffer_ty *B, buffer_ty *C, size_t size) {
#pragma HLS INTERFACE m_axi depth = DEPTH_ port = A bundle = gmem0 max_read_burst_length = 64 max_write_burst_length = 64
#pragma HLS INTERFACE m_axi depth = DEPTH_ port = B bundle = gmem1 max_read_burst_length = 64 max_write_burst_length = 64
#pragma HLS INTERFACE m_axi depth = DEPTH_ port = C bundle = gmem0 max_read_burst_length = 64 max_write_burst_length = 64
#pragma HLS INTERFACE s_axilite port = size
#pragma HLS INTERFACE s_axilite port = return

  // Let us represent our pointers by elements of 512 bits, so we read more
  // memory each time
  wide_ty *wide_A = reinterpret_cast<wide_ty *>(A);
  wide_ty *wide_B = reinterpret_cast<wide_ty *>(B);
  wide_ty *wide_C = reinterpret_cast<wide_ty *>(C);

  // Original definitions
  const int bt_bits = sizeof(buffer_ty) * 8;
  const int bt_bytes = sizeof(buffer_ty);

  // New definitiones
  const int wide_bits = sizeof(wide_ty) * 8;
  const int wide_bytes = sizeof(wide_ty);

  // How many of our elements are inside the new representation
  const int num_ele = wide_bytes / bt_bytes;

  // Redefine size
  int wide_size = (size * bt_bytes) / wide_bytes;

  // Do the addition
  for (size_t i = 0; i < wide_size; i++) {
#pragma HLS pipeline II=1
    wide_ty A_wv = wide_A[i];
    wide_ty B_wv = wide_B[i];
    wide_ty C_wv = 0;

    // Unroll the addition of the read memory
    for (int j = 0; j < num_ele; j++) {
#pragma HLS unroll factor = num_ele
      int end = (j + 1) * bt_bits - 1;
      int start = j * bt_bits;

      // Do the sum
      C_wv.range(end, start) = A_wv.range(end, start) + B_wv.range(end, start);
    }

    // Put result in C
    wide_C[i] = C_wv;
  }
}
