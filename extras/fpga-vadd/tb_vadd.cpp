/**
 * Author: Pedro Henrique Rosso
 */

#include <iostream>

#include "vadd.h"

int main(int argc, char *argv[]) {
  buffer_ty A[DEPTH_];
  buffer_ty B[DEPTH_];
  buffer_ty C[DEPTH_];

  for (int i = 0; i < DEPTH_; i++) {
    A[i] = i;
    B[i] = i + 1;
  }

  vadd_hw(A, B, C, DEPTH_);

  bool fail = false;
  for (int i = 0; i < DEPTH_; i++) {
    if (A[i] + B[i] != C[i]) {
      fail = true;
      break;
    }
  }

  if (fail) { 
    std::cout << "TEST FAILED" << std::endl;
    return 1;
  } else {
    std::cout << "TEST SUCCESS" << std::endl;
    return 0;
  }
}