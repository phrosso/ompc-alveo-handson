## **OMPC + ALVEO Basic usage**
This is intended to show the basic usage of ALVEO with OMPC.
- One can use ALVEO standalone also. It is made for a single node, and it does not require running with `mpirun`
  - Alveo plugin also implements one instance of x86-64 cpu plugin, so it can offload CPU kernels if there is a non matching kernel on the FPGA
- The usage of OMPC with ALVEO is very similar to OMPC usage. But now, we specify kernels in the FPGA
- We assume that an bitstream for FPGA is present **(bitstreams are the FPGA binaries, and have the file extension .xclbin)**. A bitstream with element-wise vector addition is provided (source code is available in the [FPGA Vadd](../extras/fpga-vadd) folder).

### **Refer to [Compiling ALVEO + OMPC](../readme.md#compiling-ompc-and-ompcalveo-fork) before going through this part**

### **Quick Start**
To directly run the examples, assuming having OMPC + ALVEO built:

```bash
# Make sure env.sh is properly set
source env.sh
make
make test             # launch with OMPC
make test-standalone  # launch Alveo Plugin without OMPC (single node only)
```

### **Environment**
To run OMPC + ALVEO, we need to set MPI (if not by default), OMPC to the PATHs (env.sh). We also have to source the AMD/Xilinx tools (the tools provide scripts we can source. The script is usually found on /opt/xilinx/xrt/setup.sh)
```bash
# MPI
MPI_ROOT=<path_to_mpi_instalation>
export PATH=$MPI_ROOT/bin:$PATH
export LD_LIBRARY_PATH=$MPI_ROOT/lib:$LD_LIBRARY_PATH
# OMPC
OMPC_ROOT=<path_to_llvm_project>
export CXX=$OMPC_ROOT/build/bin/clang++
export LD_LIBRARY_PATH=$OMPC_ROOT/build/lib/:$OMPC_ROOT/build/projects/openmp/libomptarget:$LD_LIBRARY_PATH
export LIBRARY_PATH=$OMPC_ROOT/build/lib/:$OMPC_ROOT/build/projects/openmp/libomptarget:$LIBRARY_PATH
export CPATH=$OMPC_ROOT/build/projects/openmp/runtime/src:$CPATH
# AMD/Xilinx tools
source /opt/xilinx/xrt/setup.sh
```

### **Element-Wise Vector Addition**
Let's say we have the kernel `vadd_hw` on our FPGA bitstream file, which has the same prototype as the CPU kernel we made before. The only addition we will make is to declare the FPGA kernel as variant for the CPU kernel, specifying the `device = {arch(alveo)}` as condition to the variant. 

```c++
void vadd_hw(unsigned int *A, unsigned int *B, unsigned int *C, size_t size);
#pragma omp declare variant(vadd_hw) match(device = {arch(alveo)})
void vadd(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++)
    C[i] = A[i] + B[i];
}
```
OMPC uses the asynchronous target functions inside a parallel single region. Only at the end of the single region that the task execution starts. This permits OMPC to have the task-graph of the region to be able to schedule the tasks according to the nodes launched. 

Important to notice that the ALVEO plugin needs to know where to search for FPGA bitstream. By default it looks at the relative path to the application `(./)`. The most easy way to change that is to use the `OMPTARGET_ALVEO_XCLBIN_DIR` environment variable.

To run the code: **(We always launch OMPC with `mpirun`, that is how libomptarget detect we are using OMPC. We always use 1 process for the head node, and the rest for OMPC worker nodes When using with FPGAs, match the number of workers with the number of FPGAs).**
```bash
source env.sh
$CXX -fopenmp -fopenmp-targets=alveo -fno-openmp-new-driver vadd.cpp -o vadd
# Running with OMPC
mpirun -np 2 ./vadd
# Running Standalone (1 machine, no OMPC)
./vadd
```

**PS-1:** To see what is happening inside the ALVEO plugin one can enable the plugin debug messages:

- `-DLIBOMPTARGET_ENABLE_DEBUG=On` during the compilation of the LLVM fork
- `export ALVEO_DEBUG=1` before program execution

**PS-2:** If wanting to compare how would we execute the same program on classic [FPGA API](../extras/fpga-host/).


### **More details**
#### **ALVEO**
The Alveo plugin does not yet have a documentation, but it is coordinated by few environment variables:

| **Environment Variable**     	| **Value** 	| **Description**                                                                                                                                                                                                                       	|
|------------------------------	|-----------	|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| `ALVEO_DEBUG`                  	| int       	| If defined as 1, enables debug messages from the plugin (debug has to be enable at fork compile time)                                                                                                                                 	|
| `OMPCLUSTER_DISABLE_FPGA`      	| int       	| If defined as 1 it disables OMPC to run with FPGAs. This is used when building the OMPC fork with ALVEO plugin together and there is no intention to use the FPGAs. Not sourcing the XRT environment file has the same behavior       	|
| `OMPTARGET_ALVEO_XCLBIN_DIR`   	| string    	| If defined overwrites the default path (./) for looking for FPGA bitstream                                                                                                                                                            	|
| `OMPTARGET_ALVEO_XCLBIN_DESC`  	| string    	| If defined, points to a file which describes (line by line) the PATH for the bitstream file for each FPGA in the system (should match the number of FPGAs user wants). Overwrites any path that was set for searching FPGA bitstreams 	|
| `OMPTARGET_ALVEO_BOARDS_NODE`  	| int       	| Specifies the number of FPGAs per node. This helps OMPC understands how it should distribute each node FPGAs on MPI processes launched. OMPC + ALVEO uses 1 MPI process per FPGA in the system                                        	|
| `OMPTARGET_ALVEO_ACCL_IPS`     	| string    	| Specifies the path for the file that describes (line by line) the IPs of each FPGA in the system. This will be used only if ACCL is active.                                                                                           	|
| `OMPTARGET_ALVEO_DISABLE_ACCL` 	| int       	| If defined as 1 and the Alveo plugin was built with ACCL support, will disable the ACCL usage and will fallback to OMPC MPI to move data between FPGAs.                                                                               	|

#### **ACCL**
[ACCL](https://github.com/Xilinx/ACCL/tree/dev) is the Alveo Collective Communication Library. It is the focus of the collaboration between OMPC and AMD/Xilinx. If the OMPC + ALVEO is built with ACCL support, and the system is capable (a switch connecting the FPGAs in the system) all the communication done between FPGAs will be done using ACCL, communicating direct from FPGA to another with needing to move data back to FPGA's hosts.

More details about that will be happily given if requested.