## **Distributed Computing Using OMPC**

OMPC was made to be a distributed offload plugin. So let's make the vector addition application more complex so we can see what OMPC is capable, and how we can leverage that to distribute work to several FPGAs.

**Please, make sure `hosts` file is correctly reflecting the node usage**

### **Refer to [Compiling ALVEO + OMPC](../readme.md#compiling-ompc-and-ompcalveo-fork) before going through this part**

### **Quick Start**
**It is advised to go to [/ompc](/ompc) and [/ompc-alveo](/ompc-alveo) before running these examples.**

To directly run the examples, assuming having OMPC + ALVEO built:

```bash
# This is a distributed example, make enverything is set on every server used

# Make sure env.sh is properly set
source env.sh
make            # this will compile all examples (if the fork build is capable)
```
```bash
# Some examples have OMPC data management, others no. Refer to sections bellow for explanation
make test       # to run OMPC with no data management
make test-dm    # to run OMPC with data management

source /opt/xilinx/xrt/setup.sh
make fpga       # to run OMPC + ALVEO with no data management
make fpga-dm    # to run OMPC + ALVEO with data management
```
**PS:** If the fork was build with both plugins, and the execution detects that FPGAs are availabe, it will try to use few things about ALVEO plugin. To disable ALVEO plugin and run on regular OMPC one should do `unset XILINX_XRT` or `export OMPCLUSTER_DISABLE_FPGA=1`.

### **4-vector element-wise reduction**
- Assuming we have vector from A to G, we want to:
- C=A+B; F=D+E; and G=C+F

One way we could do that with OMPC was by creating few target tasks with dependencies

```cpp
#pragma omp parallel
#pragma omp single
  {
  #pragma omp target map(to: A[:size], B[:size]) map(tofrom: C[:size]) \
                   depend(in: *A, *B) depend(inout: *C) nowait
    vadd(A, B, C, size);
  #pragma omp target map(to: D[:size], E[:size]) map(tofrom: F[:size]) \
                   depend(in: *D, *E) depend(inout: *F) nowait
    vadd(D, E, F, size);
  #pragma omp target map(to: C[:size], F[:size]) map(tofrom: G[:size]) \
                   depend(in: *C, *F) depend(inout: *G) nowait
    vadd(C, F, G, size);
  }
```

The main issue with that piece of code is that there we are moving data from/to head node all the time, which can be expensive in a distributed system. 

To avoid that, we use the OMPC **data manager**. Looking at the code, vectors **C** and **F** will be moved from one worker to another. If we declare them as data entries for the duration of the region, they will be directed forwarded from one worker to another when needed. 

We can also now map them 'implicitly'. It is **very important** that programmer manages the dependencies correctly. **(notice that OMPCLUSTER_DEBUG now will show 'Exchange' events, that represents the forwarding data from one worker to another)**.

The code that uses data manager is:
```cpp
#pragma omp parallel
#pragma omp single
  {
  #pragma omp target enter data map(to: C[:size]) depend(inout: *C) nowait
  #pragma omp target enter data map(to: F[:size]) depend(inout: *F) nowait

  #pragma omp target map(to: A[:size], B[:size])                        \
                  depend(in: *A, *B) depend(inout: *C) nowait
    vadd(A, B, C, size);
  #pragma omp target map(to: D[:size], E[:size])                        \
                  depend(in: *D, *E) depend(inout: *F) nowait
    vadd(D, E, F, size);
  #pragma omp target map(tofrom: G[:size])                              \
                  depend(in: *C, *F) depend(inout: *G) nowait
    vadd(C, F, G, size);

  #pragma omp target exit data map(release: C[:size]) depend(inout: *C) nowait
  #pragma omp target exit data map(release: F[:size]) depend(inout: *F) nowait
  }
```

### **Running**
Running this example requires at least two machines. We use 3 MPI processes (1 head node and 1 worker node). Here, we put the head node in the same node of the first worker. Compilation of the application is the same as other examples.

```bash
# Make sure env.sh is set, and that hosts file is properly set
mpirun -np 3 -f ./hosts ./<executable>
```

### **What about FPGAs**

Putting the declare variant the same way we did in the basic example should be enough to run that on FPGAs, the application **DOES NOT** change! (besides the addition of the `declare variant`)


**PS-1:** These examples are intended to be distributed so we can see data movement between OMPC workers

**PS-2:** The compilation of these programs are done the same way we done in the other parts of this hands on