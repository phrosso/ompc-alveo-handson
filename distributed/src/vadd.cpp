#include <stdio.h>
#include <stdlib.h>

void vadd(unsigned int *A, unsigned int *B, unsigned int *C, size_t size) {
  for (int i = 0; i < size; i++)
    C[i] = A[i] + B[i];
}

void init_array(unsigned int *V, size_t size, unsigned int value) {
  for (int i = 0; i < size; i++)
    V[i] = value;
}

bool validate(unsigned int *A, unsigned int *B, unsigned int *D,
              unsigned int *E, unsigned int *G, size_t size) {
  for (int i = 0; i < size; i++)
    if (G[i] != (A[i] + B[i] + D[i] + E[i]))
      return false;
  return true;
}

int main() {
  size_t size = 1024;

  unsigned int *A = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(A, size, 1);
  unsigned int *B = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(B, size, 2);
  unsigned int *C = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(C, size, 0);
  unsigned int *D = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(D, size, 3);
  unsigned int *E = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(E, size, 4);
  unsigned int *F = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(F, size, 0);
  unsigned int *G = (unsigned int *)malloc(sizeof(unsigned int) * size);
  init_array(G, size, 0);

#pragma omp parallel
#pragma omp single
  {
#pragma omp target map(to: A[:size], B[:size]) map(tofrom: C[:size]) \
                   depend(in: *A, *B) depend(inout: *C) nowait
    vadd(A, B, C, size);
#pragma omp target map(to: D[:size], E[:size]) map(tofrom: F[:size]) \
                   depend(in: *D, *E) depend(inout: *F) nowait
    vadd(D, E, F, size);
#pragma omp target map(to: C[:size], F[:size]) map(tofrom: G[:size]) \
                   depend(in: *C, *F) depend(inout: *G) nowait
    vadd(C, F, G, size);
  }

  if (!validate(A, B, D, E, G, size)) {
    printf("Execution Failed\n"); 
    return 1;
  } else {
    printf("Execution Succedeed\n"); 
    return 0;
  }
}