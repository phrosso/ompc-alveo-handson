## **Basic Usage of OMPC + FPGA**

## **Terminology**

- `OMPC` refers to examples and information related to OpenMP Cluster (OMPC)
- `OMPC + ALVEO` refers to examples and information about the Alveo Plugin for OpenMP used with OMPC 

## **Useful Links:** 

- OMPC Paper: https://arxiv.org/pdf/2207.05677.pdf
- OMPC Repository: https://gitlab.com/ompcluster/llvm-project 
- OMPC ReadTheDocs: https://ompcluster.readthedocs.io/en/latest/ 
- ALVEO Repository:  https://gitlab.com/ompcluster/llvm-project/-/tree/fpga/alveo-plugin?ref_type=heads

#### **Please set the correct paths on env.sh files** before using these examples

## **Introduction**
Each one of the folders in the repository contains a readme.md file that provides useful information. 

Preferable follow the indicated Repo Structure to make the best of this examples repository

## **Repo Structure**
### **OMPC and OMPC + ALVEO**
- Refer to [`/ompc`](./ompc/) folder for basic OMPC usage
- Refer to [`/alveo`](./alveo/) folder for basic ALVEO / OMPC+ALVEO usage
- Refer to [`/distributed`](./distributed/) to go advanced and distributed using OMPC and OMPC + ALVEO

### **Extras**
- Refer to [`extras/fpga-vadd`](./extras/fpga-vadd/) for a basic HLS implementation of a FPGA kernel
- Refer to [`extras/fpga-host`](./extras/fpga-host/) for a basic single FPGA XRT API only code (for comparision with OpenMP application for FPGAs).

## **AMD/Xilinx Tools Overview**
When working with FPGAs, usually we will have two flows:

### **Deployment**
To launch applications using FPGAs, assuming the existence of one FPGA bitstream. One will need:

- Xilinx Runtime (XRT) (https://www.xilinx.com/products/design-tools/vitis/xrt.html)
- Deployment Target platform (dependent on the target board)

### **Development**
To be able to program FPGA kernels using HLS, one will need:

- Xilinx Runtime (XRT) (https://www.xilinx.com/products/design-tools/vitis/xrt.html)
- Vitis Unified Software Platform (https://www.xilinx.com/products/design-tools/vitis/vitis-platform.html)
- Development Target platform  (dependent on the target board)

**PS:** The easiest way to obtain all this software for your system is to go to the desired platform and look for your system specifications. (e.g.: For u55c FPGA: https://www.xilinx.com/products/boards-and-kits/alveo/u55c.html#gettingStarted)

**Link to Alveo FPGAs:** https://www.xilinx.com/products/boards-and-kits/alveo.html

### **Versions we use**
The versions we normally use are:

- Xilinx Runtime (XRT) - 2.13.466 or 2.14
- Vitis Unified Software Platform - 2022.1
- Deployment Target platform - Chosen from https://www.xilinx.com/products/boards-and-kits/alveo/u55c.html#gettingStarted based on the XRT and Vitis versions
- Development Target platform - Chosen from https://www.xilinx.com/products/boards-and-kits/alveo/u55c.html#gettingStarted based on the XRT and Vitis versions

## **OMPC + ALVEO Dependencies**
To build our LLVM fork, we usually use a pre-built LLVM Clang compiler. Usually we go with:

- **[LLVM Clang 12.0](https://github.com/llvm/llvm-project/releases/tag/llvmorg-12.0.1)** (export CC as clang-12 and CXX as clang++-12 variants)

### **OMPC Specifics**
- **MPI Distribution**
  - Normally we build OMPC against a MPICH distribution
- **(Optional) [Veloc 1.4](https://github.com/ECP-VeloC/VELOC/releases/tag/veloc-1.4)**
  - This is only used for Fault-Tolerance support. It is not a must have requirement.

### **ALVEO Specifics**
- **Xilinx Runtime (XRT)**
  - Alveo Plugin is currently based on XRT version 2.13.466 (run on newer versions)
- **Deployment Target Platform** 
  - Refers to the board that will be used
- **(Optional) [ACCL](https://github.com/Xilinx/ACCL/tree/dev)**
  - ACCL is the Alveo Collective Communication Library. It is used for transfers between FPGAs

### **Repositories**
- OMPC and ALVEO are developed on top of the same LLVM fork: https://gitlab.com/ompcluster/llvm-project
- Mostly of branches there are dedicated to the OMPC fork
- The ALVEO work is kept separated of the main OMPC development in the fpga/alveo-plugin branch: https://gitlab.com/ompcluster/llvm-project/-/tree/fpga/alveo-plugin?ref_type=heads

## **Compiling OMPC and OMPC+ALVEO fork**
Assuming the dependencies were set to the PATH, LD_LIBRARY_PATH, LIBRARY_PATH and CPATH

Building our LLVM fork for OMPC and for ALVEO require the same steps. The choice of building each plugin is according to the dependencies available. 

```bash
export CC=clang-12
export CXX=clang++-12
cd llvm-project
mkdir build && cd build
cmake ../llvm -DCMAKE_BUILD_TYPE="Release" -DLIBOMPTARGET_ENABLE_DEBUG=On -DLLVM_ENABLE_ASSERTIONS=On -DLLVM_ENABLE_PROJECTS="clang;openmp" -DLLVM_TARGETS_TO_BUILD="X86" -DLLVM_USE_LINKER=gold -DLLVM_LINK_LLVM_DYLIB=On
make -j$(nproc)
```